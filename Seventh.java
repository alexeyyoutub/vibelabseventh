import java.util.ArrayList;

public class Seventh {
    public static void main(String[] args) {
        ArrayList <String> arr_list = new ArrayList<>();

        arr_list.add("first");
        arr_list.add("third");
        arr_list.add(1, "second");
        System.out.println("ArrayList:");
        for (Object o: arr_list)
            System.out.println(o);

        System.out.println("\nSize of ArrayList:" + arr_list.size());

        System.out.println("\nAfter remove:");
        arr_list.remove(1);
        arr_list.remove("third");
        for (Object o: arr_list)
            System.out.println(o);

        System.out.println("\nAfter contains(\"first\"):");
        System.out.println(arr_list.contains("first"));

        System.out.println("\nAfter get(0):");
        System.out.println(arr_list.get(0));

        arr_list.clear();

        System.out.println("\nChecking for emptiness after cleaning");
        System.out.println(arr_list.isEmpty());
    }
}
